class AddCustomersToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :customers, :integer
  end
end
