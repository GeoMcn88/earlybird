class CreateTables < ActiveRecord::Migration[5.2]
  def change
    create_table :tables do |t|
      t.integer :seats
      t.integer :restaurant_id 
      t.double :price

      t.timestamps
    end
  end
end
