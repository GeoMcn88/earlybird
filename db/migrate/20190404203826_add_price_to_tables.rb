class AddPriceToTables < ActiveRecord::Migration[5.2]
  def change
    add_column :tables, :price, :decimal
  end
end
