class AddCountryIdToRestaurant < ActiveRecord::Migration[5.2]
  def change
    add_column :restaurants, :country_id, :integer
  end
end
