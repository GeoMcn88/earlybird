# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Country.create(name: 'Ireland')

Region.create(name: 'Dublin', country_id: '1')
Region.create(name: 'Cork', country_id: '1')
Region.create(name: 'Belfast', country_id: '1')
Region.create(name: 'Galway', country_id: '1')