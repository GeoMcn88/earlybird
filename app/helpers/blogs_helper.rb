module BlogsHelper
    
    def blog_image(blog_id)
    blog = Blog.find!(blog_id)
    if blog.image.attached?
        image_tag blog.image
    else
        image_tag 'default_avatar.jpg'
    end
end
end
