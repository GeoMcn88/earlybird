class UserNotifierMailer < ApplicationMailer
    default :from => 'geomcn11@gmail.com'

  # send a signup email to the user, pass in the user object that contains the user's email address
  def send_reservation_email(user)
    mail( :to => user.email,
    :subject => 'Thanks for signing up for our amazing app' )
  end
end
