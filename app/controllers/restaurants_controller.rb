class RestaurantsController < ApplicationController
    before_action :find_restaurant, only: [:show]
    before_action :find_table, only: [:show]
    
    
    def index
        
        if params[:region].blank?
            @restaurants = Restaurant.all.order("created_at DESC")
        else
            @region_id = Region.find_by(name: params[:region]).id
            @restaurants = Restaurant.where(region_id: @region_id).order("created_at DESC")
        end
    end
    
    def show
        @user = current_user
        @reservation = Reservation.new(user_id: @user, table_id: @table, time: @time, user: @user)
        @reviews = Review.where(restaurant_id: @restaurant.id).order("created_at DESC")
    end
    
    def new
        @restaurant = Restaurant.new
    end
    
    def create
           @restaurant = Restaurant.new(restaurant_params)
         if @restaurant.save
             @table = Table.new(restaurant_id: @restaurant.id, price: 25, seats: 4)
             @table.save
             @restaurant.table_id = @table.id
             @restaurant.save

              redirect_to restaurants_path, notice: 'The restaurant was created!'
         else 
              render 'new'
         end 
    end
    
    
    
    
private
    def restaurant_params
        params.require(:restaurant).permit(:title, :description, :region_id, :image, :table_id)
    end
    
   
    
    def find_restaurant
        @restaurant = Restaurant.find(params[:id])
    end
    
    def find_table
        @table = Table.find(params[:id] = @restaurant.id)
    end
    
end
