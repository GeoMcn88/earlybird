class Admin::ReservationsController < Admin::AdminController 
before_action :find_reservation, only: [:show, :edit, :update, :destroy]

  def index
    @reservations = Reservation.all
      
   end
    
    def show
        
    end
    
    def new
        
    end
    
    def create
        @reservation = Reservation.new(reservation_params)
     if @reservation.save
          redirect_to admin_reservations_path, notice: 'The table was created!'
     else 
          render ‘new’
     end 
    end
    
    def edit
        
    end
    
    def update
        if @reservation.update(reservation_params)
          redirect_to @reservation, notice: 'Update successful'
     else
          render ‘edit’
     end
    end
    
    def destroy
        @reservation.destroy
     redirect_to root_path, notice: 'Reservation destroyed'
    end    
    
    
    private
        def reservation_params
            params.require(:reservation).permit(:time, :customers, :user_id, :table_id)
        end

        def find_table
            @table = Table.find(params[:id])
        end

    end
end
    