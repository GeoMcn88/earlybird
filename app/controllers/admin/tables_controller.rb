class Admin::TablesController < Admin::AdminController
before_action :find_table, only: [:show, :edit, :update, :destroy]

  
    
    def index
        if params[:region].blank?
          @restaurants = Restaurant.all.order("created_at DESC")
        else
          @region_id = Region.find_by(name: params[:region]).id
          @restaurants = Restaurant.where(region_id: @region_id).order("created_at DESC")
        end
    end
    
    def show
        @restaurant = Restaurant.find(params[:id] == @table.id)
        @user = current_user
    end
    
    def new
        @table = Table.new
    end
    
    def create
        @table = Table.new(table_params)
        if @table.save
           redirect_to admin_tables_path, notice: 'The table was created!'
        else 
           render ‘new’
        end 
    end
    
    def edit
        
    end
    
    def update
        if @table.update(table_params)
          redirect_to @table, notice: 'Update successful'
        else
          render ‘edit’
        end
    end

    def destroy
        @table.destroy
        redirect_to root_path, notice: 'Table destroyed'
    end   
    
    def self.get_details
        @table = Table.find(params[:id])
    end 



private
    def table_params
        params.require(:table).permit(:price, :seats, :restaurant_id)
    end
    
    def find_table
        @table = Table.find(params[:id])
    end

    
end