class Admin::RestaurantsController < Admin::AdminController
#    before_action :find_restaurant, only: [:show, :edit, :update, :destroy]
    
    def index
      if params[:region].blank?
        @restaurants = Restaurant.all.order("created_at DESC")
      else
        @region_id = Region.find_by(name: params[:region]).id
        @restaurants = Restaurant.where(region_id: @region_id).order("created_at DESC")
      end
    end
    
    def show
    end
    
    def new
        @restaurant = Restaurant.new
    end
    
    def create
           @restaurant = Restaurant.new(restaurant_params)
         if @restaurant.save
             @table = Table.new(restaurant_id: @restaurant.id, price: 25, seats: 4)
             @table.save
             @restaurant.table_id = @table.id
             @restaurant.save

              redirect_to restaurants_path, notice: 'The restaurant was created!'
         else 
              render 'new'
         end 
    end
    
    def destroy
        @restaurant.destroy
        @table = Table.where(:restaurant_id => @restaurant.id)
        @table.destroy
        redirect_to root_path, notice: 'Restaurant destroyed'
    end 
    
    def edit
        
    end
    
    def update
        if @restaurant.update(restaurant_params)
          redirect_to @restaurant, notice: 'Update successful'
     else
          render ‘edit’
     end
    end
    
    def destroy
        @restaurant.destroy
     redirect_to root_path, notice: 'Restaurant destroyed'
    end    
    
    
private
    def restaurant_params
        params.require(:restaurant).permit(:title, :description, :region_id, :image)
    end
    
    def find_restaurant
        @restaurant = Restaurant.find(params[:id])
    end
    
    
end
