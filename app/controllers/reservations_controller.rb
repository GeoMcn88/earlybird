class ReservationsController < ApplicationController
    before_action :find_reservation, only: [:show, :edit, :update, :destroy]
    before_action :find_expired_reservations, only: [:index]
    def index
        @reservations = Reservation.where( :user_id => current_user.id)
                                   .where("time >= ?", Date.yesterday).order("time ASC")
    end
    
    def show
    end
    
    def new
        @reservation = Reservation.new
    end
    
    def create
        @reservation = Reservation.new(reservation_params)
        @restaurant = Restaurant.find(@reservation.table.restaurant_id)
        if @reservation.save
            UserNotifierMailer.send_reservation_email(current_user).deliver
           redirect_to @reservation, notice: 'Success'
            else
            flash[:error] = "Reservation unsuccessful. Date is already reserved or not enough seats available."
              redirect_to @restaurant
        end
    end
    
    def edit
        
    end
    
    def update
         if @reservation.update(reservation_params)
              redirect_to @reservation, notice: 'Update successful'
         else
              render 'edit'
         end
    end
    
    def destroy
        @reservation.destroy
        redirect_to reservations_path, notice: 'Reservation destroyed'
    end    
    
    
private
    def reservation_params
        params.require(:reservation).permit( :customers, :time, :user_id, :table_id)
    end
    
    def find_reservation
        @reservation = Reservation.find(params[:id])
    end
    
    def find_expired_reservations
        @expired_reservations = Reservation.where("time <= ?", Date.yesterday)
                                            .where("user_id = ?", current_user.id ) 
    end
    
end
