class ReservationValidator < ActiveModel::Validator
  def validate(reservation)
      @reserved_times = Reservation.where( :table_id => reservation.table_id)
      @max_seats = Table.where( :id => reservation.table_id)
      @max_seats.each do |s|
      @reserved_times.each do |r|
    if reservation.time == r.time || reservation.customers > s.seats
      reservation.errors[:time] << "Time is taken"
    end
      end
      end
  end
end

class Reservation < ApplicationRecord
    belongs_to :table
    belongs_to :user
    validates_with ReservationValidator, on: :create
    
    validates_uniqueness_of :time
end
