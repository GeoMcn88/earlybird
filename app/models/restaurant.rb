class Restaurant < ApplicationRecord
    belongs_to :region
    belongs_to :country
    has_one_attached :image
    validates :title, :description, :region_id, presence: true
    has_many  :line_items, inverse_of: :orders 
    has_one :tables, dependent: :destroy
    has_many :reviews, dependent: :destroy
    
    validates_uniqueness_of :title
end
