class Table < ApplicationRecord
    belongs_to :restaurant
    has_many :reservations
    
    validates :seats, presence: true
    validates :price, presence: true
end
