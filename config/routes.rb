Rails.application.routes.draw do
  
  devise_for :users, :controllers => { registrations: 'registrations', confirmations: 'confirmations' }
  
  namespace :admin do 
      resources :restaurants
  end
    
  resources :blogs 
  resources :restaurants do
      resources :reviews, except: [:show, :index]
      
  end
    
    resources :reservations
    resources :tables
    resources :reviews
    
    
    
    
    root 'restaurants#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    
end
